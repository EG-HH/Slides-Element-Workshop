# Element: Ein Matrix-Messenger

## Überblick

1. Was ist Matrix? Und warum?
2. Einrichtung & Server
3. Gruppen-Tipps
4. Sitzungen & Verifikation

_Stand: 14.11.2022_

_Quelle: [https://codeberg.org/EG-HH/Slides-Element-Workshop](https://codeberg.org/EG-HH/Slides-Element-Workshop)_


## Einführung

### Namenschaos

* __Matrix__: Kommunikationsweg / Netzwerk (wie "E-Mail")
* __Element__: Programm / App (wie "Thunderbird")
    * hieß früher __Riot__
* __matrix.org__: ein Anbieter (wie "posteo.de")
  
### Günde für Matrix

* Verschlüsselt
* Pseudonym nutzbar
* Keine Verknüpfung mit Telefonnummern
* Apps für Handy, PC oder Browser
* Dezentrales Netzwerk

### Matrix-ID: `@name:server.tld`

* ähnlich zu E-Mail-Adressen: `name@server.tld`

## Dezentralisierung

![Dezentrales Matrix-Netzwerk](matrix-netzwerk.png)

## Heimserver aussuchen

### Kriterien
* Werte & Regeln
* Datenschutzmaßnahmen
	* Zusammenarbeit mit Strafverfolgungsbehörden
	* Standort & Gesetzeslage
	* Größe & Attraktivität für Repression
* Zuverlässigkeit
* eventuelle Gebühren

### Empfehlungen
* Radikale Technik-Kollektive
	* z.B. [`systemli.org`](https://systemli.org), [`systemausfall.org`](https://systemausfall.org), [`immerda.ch`](https://immerda.ch)
	* benötigen meist eine Einladung
* mehr Links: [https://pad.kanthaus.online/s/Matrix-Einfuehrung](https://pad.kanthaus.online/s/Matrix-Einfuehrung#1-Einen-Server-ausw%C3%A4hlen)

## Was ist verschlüsselt?

* Verschlüsselt: Nachrichten**inhalte**
	* Texte, Bilder, Videos, ...
* Unverschlüsselt: sehr viele **Metadaten**
	* Profildaten, Matrix-ID, Anzeigename, Profilbild, ...
	* Wer ist online? Wer hat welche Nachrichten gelesen?
	* Wer hat wann wem geschrieben? Wer hat auf welche Nachricht reagiert / geantwortet?
	* Raumname, Raumthema, Mitgliederliste, Widgets
* Die Daten für einen Raum werden auf allen beteiligten Heim-Servern gespeichert.
	* Konzentriert eure Gruppe ggf. auf wenige Server
* Ausprobieren:
	* Einloggen im Browser ("privates"/"Inkognito"-Fenster)
	* **nicht** verifizieren

## Einrichtung

* **Anleitung**: [http://ende-gelaende-hamburg.info/technik](http://ende-gelaende-hamburg.info/technik)
* **Registrieren**, z.B. bei systemli.org:
	1. Account erstellen unter [https://users.systemli.org/](https://users.systemli.org/)
		* Einladungscode benötigt
	2. Login in Element mit Homeserver "`systemli.org`"


## Gruppen - Einstellungen

* **Verschlüsselung**: an / aus
* **Zugang**:
	* privat (per Einladung)
	* Öffentlich (per Link / Adresse oder Raumverzeichnis)
	* Spacemitglieder
* **Einladungslink**: kann immer geteilt werden, aber Zugang muss erlaubt sein
* **Adresse**, um den Raum ohne Link zu finden
	* lokal (= auf deinem Heimserver) oder öffentlich (in öffentlichem Verzeichnis gelistet)
* **Sichtbarkeit des Verlaufs**:
	* öffentlich
	* nur Mitglieder ab Beitritt / ab Einladung / seit Beginn

### Voreinstellungen:
	
Raumtyp         verschlüsselt   Zugang            Adresse   Verlauf
--------------  -------------   ---------------   -------   -----------
**privat**      ja              privat            nein      seit Beginn
**öffentlich**  nein            öffentlich        ja        seit Beginn


## Gruppen - Widgets

* Einbinden von externen Inhalten direkt in Gruppe, z.B.:
	* Videokonferenzen: Jitsi, BigBlueButton
	* Pads
* Achtung: unverschlüsselt!

![Widgets hinzufügen](Widget-hinzufügen.png)

## Gruppen - Rechte

* Komplexe Berechtigungs-Einstellungen möglich
* Rollen: Standard - Moderator\*in - Administrator\*in
	* weitere Abstufungen möglich

![Rollen & Berechtigungen](Raum-Rollen.png)


## Sitzungen & Verifikation

* **1 Sitzung** = 1 Login auf 1 Gerät, z.B.
	* Element-App auf dem Handy
	* Element-App auf dem PC
	* Element-Webseite im Browser

* **Verifikation**:
	* Identität bestätigen
	* Zusätzlicher Schutz bei gehacktem Passwort:
	  ohne Verifikation kein Zugriff auf (alte) verschlüsselte Nachrichten
	
## Login: Eigene Sitzung verifizieren

* mit anderer Sitzung
  *oder*
* mit Sicherheitsschlüssel (Backup nötig!)

**Ausprobieren**:

* Einloggen im Browser ("privates"/"Inkognito"-Fenster)

![Anmeldung verifizieren](Anmeldung-verifizieren.png){ width=250px }

## Andere Personen verifizieren

* Notwendig, um sicherzustellen, dass Nachrichten an richtige Person verschlüsselt werden
* geht per Emoji-Vergleich oder QR-Code
* Verifizierte Personen bekommen grünes Schildchen mit Haken

![Nicht verifizierte Sitzung](Alice-unverifiziert.png){ width=250px }

## Backup einrichten

![Backup einrichten](Backup-einrichten.png)

* Sicherheitsschlüssel generieren lassen & speichern (am besten im Passwortmanager)

## Server-Einstellungen

* **Integrations-Server**:
	* Bindet "Widgets" (Pads, Konferenzen, ...) in Gruppen ein
	* Dein Heim-Server bestimmt, welchen Integrations-Server du nutzt.
	* z.B. `dimension.systemli.org/element`
* **Identitäts-Server**:
	* Hilft Menschen, dich anhand von Mailadresse / Telnr. zu finden
	* Nicht benötigt
	
![](Server-Einstellungen.png){width=350px}

## Fehlerbehebung

* Neu einloggen
* Einstellungen $\rightarrow$ Hilfe & Über $\rightarrow$ Zwischenspeicher löschen
* Developer Tools öffnen (`Strg+Shift+I`) \
  $\rightarrow$ Neu laden (`Strg+Shift+R`)
