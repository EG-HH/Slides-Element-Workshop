# Präsentation für einen Workshop zum sicheren Chatten per Matrix

## Präsentation bearbeiten

Die Datei [`Element-Workshop.md`](Element-Workshop.md) enthält die Folien im Markdown-Format.

## PDF erstellen

Um aus der Markdown-Datei `Element-Workshop.md` eine PDF-Präsentation zu generieren, wird pandoc verwendet.

Installationshinweise zu pandoc gibt es hier: [https://pandoc.org/installing.html]

Mit diesem Befehl wird das PDF erzeugt:

```
pandoc -t beamer -o Element-Workshop.pdf Element-Workshop.md
```
